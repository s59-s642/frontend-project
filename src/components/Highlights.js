import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				 <Card.Img variant="top" src="./images/400ML.jpg" />
				     <Card.Body>
				       <Card.Title>Water Bottles</Card.Title>
				       <Card.Text>
				       Shop our wide range of BPA/BPS free water bottles.
				       </Card.Text>
				     </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Bottle Accessories</Card.Title>
				       <Card.Text>
				         Want more for your water bottle? Shop our wide range of water accessories.
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Wide Mouth Water Bottles</Card.Title>
				       <Card.Text>
				         Our iconic Wide Mouth water bottles are perfect for everyday.
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>
		</Row>
			
	)
};