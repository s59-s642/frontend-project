import { useState, useEffect, useContext } from 'react';
import { Modal, Button, Form, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {
	const { user, setUser } = useContext(UserContext);

	const [show, setShow] = useState(false)
	const [isActive, setIsActive] = useState(false)

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [quantity, setQuantity] = useState("")

	const navigate = useNavigate()

	const clearForm = () => {
		setName("")
		setDescription("")
		setPrice("")
		setQuantity("")
	}

	function addProduct (e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}products/addProduct`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Product Added!",
					icon: "success",
					text: "Product successfully added!"
				})
				navigate('/productMaintenance')
			} else {
				Swal.fire({
					title: "Product NOT Added",
					icon: "info",
					text: "Please check product information"
				})
			}
		})

		clearForm()
	}

	useEffect(() => {
		if(name !=="" && description !== "" && price !== "" && quantity !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [name, description, price, quantity])
	
	return (
	<>
		<div className= "d-grid gap-2 mt-4">
		<Button size="md" onClick={() => {setShow(true); clearForm()}}>Add Product</Button>
		</div>	
		<Modal className= '' size="lg" show={show} onHide={() => setShow(false)} aria-labelledby="example-modal-sizes-title-sm">
			<Modal.Header closeButton>
				<Modal.Title id="example-modal-sizes-title-sm">Add Product Form</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={name}
				    	onChange={(e) => {setName(e.target.value)}}
				    	placeholder="Enter Product Name" 
				    	required
				    	/>
				  </Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Description</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={description}
				    	onChange={(e) => {setDescription(e.target.value)}}
				    	placeholder="Enter Product Description" 
				    	required
				    	/>
				</Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Price</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={price}
				    	onChange={(e) => {setPrice(e.target.value)}}
				    	placeholder="Enter Product Price" 
				    	required
				    	/>
				</Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Quantity</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={quantity}
				    	onChange={(e) => {setQuantity(e.target.value)}}
				    	placeholder="Enter Product Quantity" 
				    	required
				    	/>
				</Form.Group>		
		</Modal.Body>


		<Modal.Footer>
	      { (isActive) ?
  			 <Button variant="primary" onClick={(e) => {addProduct(e); setShow(false); window.location.reload(false)}}>
  		 	 Save Product
  			</Button>
  			:
          <Button variant="primary" onClick={(e) => {addProduct(e); setShow(false); window.location.reload(false)}}>
            Save Product
          </Button>
	      }
	      <Button onClick={() => setShow(false)}>Close</Button>
	    </Modal.Footer>
		</Modal>

	</>

	)
}