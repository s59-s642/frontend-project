// Old practice in importing components
// import Navbar from 'react-bootstrap/NavBar';
// import Nav from 'react-bootstrap/Nav';

// New practice in importing components;
import { useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	// useContext hook
	const { user } = useContext(UserContext);

	console.log(user.fullname);

	return (

		<Navbar bg="light" expand="lg">
		    <Container className="mt-3">
		      <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll" >
		        <Nav className="me-auto">
		          <Nav.Link as={Link} to="/" >Home</Nav.Link>

		          { (user.id == null) ?
		          	<>
	          			<Button className="mx-2" as={Link} to="/register" >Register</Button>
	          			<Button as={Link} to="/login" >Login</Button>
		          	</>		 
		          	:
		          	<>	   
			          { (user.isAdmin) ?
			          	<NavDropdown title="Dashboard" id="navbarMaintenance">
			          		<NavDropdown.Item as={Link} to="/orderHistory">Order History</NavDropdown.Item>
			          		<NavDropdown.Item as={Link} to="/productMaintenance">Product Maintenance</NavDropdown.Item>
			          		{/*<NavDropdown.Item as={Link} to="/userMaintenance" >Account Maintenance</NavDropdown.Item>*/}
			          	</NavDropdown>
			          :
			          	<NavDropdown title="Dashboard" id="navbarMaintenance">
				          	<NavDropdown.Item as={Link} to="/products">Products</NavDropdown.Item>
				          	<NavDropdown.Item as={Link} to="/orderHistory">Order History</NavDropdown.Item>
			          	</NavDropdown>
			          }
			          <span class="navbar-dropdown">
			          <NavDropdown alignRight title={user.fullname}>
			          		<NavDropdown.Item as={Link} to="/userProfile">Profile</NavDropdown.Item>
			          		<NavDropdown.Item as={Link} to="/logout">Logout</NavDropdown.Item>
			          </NavDropdown>
			          </span>
		          	</>
		          }
		         
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>

	)
};