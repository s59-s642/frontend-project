import { useState, useEffect, useContext } from 'react';
import { Modal, Button, Form, Container } from 'react-bootstrap';
import { Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function EditProduct({getId, getName, getDescription, getPrice, getQuantity, getStatus, setModal}) {

	const { user, setUser } = useContext(UserContext);

	const [show, setShow] = useState(true)
	const [isActive, setIsActive] = useState(false)

	const [newName, setName] = useState("")
	const [newDescription, setDescription] = useState("")
	const [newPrice, setPrice] = useState("")
	const [newQuantity, setQuantity] = useState("")
	const [newStatus, setStatus] = useState(false)

	const handleChange = () => {
		 setStatus(newStatus => !newStatus)
	}


	console.log(newStatus)
	const navigate = useNavigate()

	const clearForm = () => {
		setName("")
		setDescription("")
		setPrice("")
		setQuantity("")
	}

	function updateProduct (e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}products/updateProduct/`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: getId,
				name: newName,
				description: newDescription,
				price: newPrice,
				quantity: newQuantity,
				isActive: newStatus
			})
		})
		.then(res => res.json())
		.then(data => {
			
			if(data){
				Swal.fire({
					title: "Product Updated!",
					icon: "success",
					text: "Product successfully updated!"
				}).then(reload => window.location.reload())
				 
			} else {
				Swal.fire({
					title: "Product NOT Added",
					icon: "info",
					text: "Please check product information"
				}).then(reload => window.location.reload())
			}
		})

		clearForm()
	}

	useEffect(() => {
		if(newName !=="" && newDescription !== "" && newPrice !== "" && newQuantity !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [newName, newDescription, newPrice, newQuantity])

	return (

	<>
	<Container class="col-md-12 text-center">
		<Modal size="lg" show={() => setModal(true)} onHide={() => setModal(false)} aria-labelledby="example-modal-sizes-title-sm">
			<Modal.Header closeButton>
				<Modal.Title id="example-modal-sizes-title-sm">Update Product Form</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>ID</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={getId} disabled
				    	/>
				  </Form.Group>

				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={newName}
				    	onChange={(e) => {setName(e.target.value)}}
				    	placeholder={getName}
				    	required
				    	/>
				  </Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Description</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={newDescription}
				    	onChange={(e) => {setDescription(e.target.value)}}
				    	placeholder={getDescription}
				    	required
				    	/>
				</Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Price</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={newPrice}
				    	onChange={(e) => {setPrice(e.target.value)}}
				    	placeholder={getPrice} 
				    	required
				    	/>
				</Form.Group>

  				<Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Quantity</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={newQuantity}
				    	onChange={(e) => {setQuantity(e.target.value)}}
				    	placeholder={getQuantity}
				    	required
				    	/>
				</Form.Group>		

				<Form.Group className="mb-3" controlId="fullName">
				     <Form.Check 
						        type="switch"
						        id="custom-switch"
						        value = {newStatus}
						        defaultChecked={getStatus}
						        onChange={handleChange}
						      />
				</Form.Group>		
		

		</Modal.Body>

		<Modal.Footer>
	      { (isActive) ?
  			 <Button variant="primary" onClick={(e) => {updateProduct(e); setModal(false);}}>
  		 	 Save Product
  			</Button>
  			:
          <Button variant="primary" onClick={(e) => {updateProduct(e); setModal(false);}} disabled>
            Save Product
          </Button>
	      }
	      <Button onClick={() => setModal(false)}>Close</Button>
	    </Modal.Footer>
		</Modal>
	</Container>
	</>
	)
}