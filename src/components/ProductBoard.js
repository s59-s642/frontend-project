import { useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductBoard({getProducts}){
	const {user} = useContext(UserContext)

	const {_id, name, description, price, quantity} = getProducts


	return (
		<Row>
      <Col lg={{span:6, offset:3}}>
        <Card className="courseCard my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
            <Card.Subtitle>Quantity:</Card.Subtitle>
              <Card.Text>{quantity}</Card.Text>
              <Button className="bg-primary" as={Link} to={`/viewProduct/${_id}`}>Details</Button>
              </Card.Body>
          </Card>
      </Col>
    </Row>
		)
}