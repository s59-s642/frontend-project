import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Table, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import UpdateProduct from './UpdateProducts'

export default function ProductDashboard() {

	const {user} = useContext(UserContext)
	const [allProd, setAllProd] = useState([])

	const [show	, setShow] = useState(false)
	const [id, setId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [quantity, setQuantity] = useState("")

	const [status, setStatus] = useState(false)

	const viewAllProducts = () =>  {

		fetch(`${process.env.REACT_APP_API_URL}products/allProducts`, {
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllProd(data.map(product => {
				return(
					<tr key={product._id}>
						{/*<td>{product._id}</td>*/}
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.quantity}</td>
						<td>{(product.isActive) ? "Active" : "Inactive"}</td>
						<Button onClick={() => {
							setShow(true); 
							setId(product._id);
							setName(product.name);
							setDescription(product.description);
							setPrice(product.price);
							setQuantity(product.quantity)
							setStatus(product.isActive)}}>Update</Button>
						
						{(show === true)?
							<div>
							 <UpdateProduct key={product._id} 
							 			getId={id}
							 			getName={name}
							 			getDescription={description}
							 			getPrice={price}
							 			getQuantity={quantity}
							 			getStatus={status} 
							 			setModal={setShow}/>

							</div>
						:
						<>
						</>}	
						
					</tr>
					
					)
				
			}))

		})

	}

	useEffect(() => {
		viewAllProducts()
	},[show])

	return (
		(user.isAdmin)
		?
		<>
			<div class=" mt-5 mb-3 text-center">
			<Card>
		      <Card.Body>
					<Table className="market-table">
				     <thead>
				       <tr>
				         {/*<th>Product ID</th>*/}
				         <th>Product Name</th>
				         <th>Description</th>
				         <th>Price</th>
				         <th>Quantity</th>
				         <th>Status</th>
				         <th>Action</th>

				       </tr>
				     </thead>
				     <tbody>
					 	{ allProd }
					 	</tbody>
				   </Table>				    
				</Card.Body>
				</Card>

		   </div>
		</>
		:
		<Navigate to="/productMaintenance" />

		)
}