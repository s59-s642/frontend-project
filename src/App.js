// react components/ dependencies
// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container, Col } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
// components
import AppNavbar from './components/AppNavbar';
// users pages
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import UserProfile from './pages/UserProfile'
import OrderHistory from './pages/OrderHistory'
// admin pages
import ProductMaintenance from './pages/ProductMaintenance';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import Checkout from './pages/Checkout';

import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    // Initial state
    id: null,
    isAdmin: null,
    fullname: null
  });

  // Function fo clearing the localStorage upon logout.
  const unsetUser = () => {
    localStorage.clear();
  }


  // This side effect is triggered by once the user signs in
  useEffect(() => {

      fetch(`${process.env.REACT_APP_API_URL}users/checkAccess`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {

          // user is logged in
          if(typeof data._id !== "undefined") {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                fullname: data.fullname
            })
          } else { // user is logged out

            setUser({
              id: null,
              isAdmin: null,
              fullname: null
            })

          }
      })

  }, []);


  return (

    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar/>
            <Container>
              <Routes>
                  <Route path="/" element={<Home/>} />
                  <Route path="/productMaintenance" element={<ProductMaintenance/>} />
                  <Route path="/products" element={<Products/>} />
                  <Route path="/viewProduct/:productId" element={<ViewProduct/>} />
                  <Route path="/checkout" element={<Checkout/>} />
                  <Route path="/orderHistory" element={<OrderHistory/>} />
                  <Route path="/userProfile" element={<UserProfile/>} />
                  <Route path="/login" element={<Login/>} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="/register" element={<Register/>} />
                  <Route path="*" element={<Error/>} />
              </Routes>
            </Container>
        </Router>
    </UserProvider>
    
  );
}

export default App;
