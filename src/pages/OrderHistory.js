import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Table, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductDashboard() {

	const {user} = useContext(UserContext)
	const [userOrder, setUserOrder] = useState([])
	const [allOrder, setAllOrder] = useState([])

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [quantity, setQuantity] = useState("")
	const [fullname, setFullName] = useState("")
	const [userId, setUserId] = useState("")
	const [userEmail, setUserEmail] = useState("")
	
	const [status, setStatus] = useState(false)

	const userOrderHistory = () =>  {

		fetch(`${process.env.REACT_APP_API_URL}users/userOrderDetails`, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUserOrder(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.product[0].name}</td>
						<td>{order.product[0].quantity}</td>
						<td>{order.product[0].price}</td>
						<td>{order.totalAmount}</td>
						<td>{order.purchasedOn}</td>	

					</tr>
					)
				
			}))

		})

	}

	const orderHistory = () =>  {

		fetch(`${process.env.REACT_APP_API_URL}users/orderDetails`, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllOrder(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.buyer[0].fullname}</td>
						{/*<td>{order.buyer[0].userId}</td>*/}
						{/*<td>{order.buyer[0].userEmail}</td>*/}
						<td>{order.product[0].name}</td>
						<td>{order.product[0].quantity}</td>
						<td>{order.product[0].price}</td>
						<td>{order.totalAmount}</td>
						<td>{order.purchasedOn}</td>				
					</tr>
					
					)
				
			}))

		})

	}

	useEffect(() => {
		userOrderHistory()
		orderHistory()
	},[])

	return (
		<div class=" mt-5 mb-3 text-center">
			<Card>
		    	<Card.Body>
		    {(user.isAdmin == true) ?
  		    		<Table className="market-table">
				    <thead>
				       <tr>
				         <th>Order ID</th>
				         <th>Buyer Fullname</th>
				         {/*<th>Buyer User ID</th> */}
				         {/*<th>Buyer Useremail</th>*/}
				         <th>Product Name</th>
				         <th>Order Quantity</th>
				         <th>Product Price</th>
				         <th>Order Total Amount</th>
				         <th>Order Date Purchased</th>
				       </tr>
				     </thead>
				    
				     <tbody>
				   	 	{ allOrder }
					 </tbody>
				   </Table>				        
			:
			  		<Table className="market-table">
				    <thead>
				       <tr>
				         <th>Order ID</th>
				         <th>Name</th>
				         <th>Quantity</th>
				         <th>Price</th>
				         <th>Total Amount</th>
				         <th>Date Purchased</th>
				       </tr>
				     </thead>
				    
				     <tbody>
				   	 	{ userOrder }
					 </tbody>
				   </Table>
			}

				</Card.Body>
			</Card>
		</div>
		)
}