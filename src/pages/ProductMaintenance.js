import { Fragment, useState } from 'react';
import AddProducts from "../components/AddProducts";
import ProductDashboard from "../components/ProductDashboard";

export default function ProductMaintenance(){
	return (
		<Fragment>
			<AddProducts/>
			<ProductDashboard/>
		</Fragment>
		)
}