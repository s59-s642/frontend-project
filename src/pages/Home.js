import { Fragment } from 'react';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard'
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "NALGENE",
		content: "Nalgene has a water bottle for every lifestyle and every adventure. Made in the USA, BPA free, durable and dishwasher safe.",
		destination: "/login",
		label: "Buy now!"
	}	

	return (
		<Fragment>
			<Banner data={data} />
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
};