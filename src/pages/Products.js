import { Fragment, useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import {Button, Form, InputGroup} from 'react-bootstrap'
import CheckProducts from "../components/ProductBoard";
import UserContext from '../UserContext';

export default function Products() {

	const {user} = useContext(UserContext);

	const [products, setProducts] = useState("")
	const [name, setName] = useState("")

	useEffect(() =>  {
			fetch(`${process.env.REACT_APP_API_URL}products/activeProducts`, {
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(res => res.json())
			.then(data => {

					setProducts(data.map(products => {
						return(
								<CheckProducts key={products._id} getProducts={products}/>
							
							)
					}))
			})
		}, [])

	
	return (
		<>
		<Fragment>
			<h1 className="text-center my-3">Products</h1>
			{products}
		</Fragment>
		</>
		)

}

