import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import CheckoutProduct from './Checkout'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ViewProduct() {

	// useContext hook for global state
	const { user } = useContext(UserContext)

	const [isActive, setIsActive] = useState(false) 
	const navigate = useNavigate(); 

	// The "useParams" hook allows us to retrieve the courseId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [newQuantity, setNewQuantity] = useState(1);


	function getProduct() {

		fetch(`${process.env.REACT_APP_API_URL}products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		
		// (show === true)?
		// 	<div>
		// 	<CheckoutProduct key={productId}
		// 	getId={productId}
		// 	getName={name}
		// 	getPrice={price}
		// 	setModal={show}/>
		// 	</div>
		// 	:
		// 	<>
		// 	</>
		})

	}

	function addOrder() {
	    fetch(`${process.env.REACT_APP_API_URL}orders/addOrders`, {
	      method: "POST",
	        headers: {
	        "Content-Type": "application/json",
	        "Authorization": `Bearer ${localStorage.getItem('token')}`
	      },
	      body: JSON.stringify({
	        buyer: [{
	          fullname: user.fullname,
	          userId: user.id,
	          userEmail: user.email
	        }],
	        product: [{
	          name: name,
	          productId: productId,
	          quantity: newQuantity,
	          price: price          
	        }]
	      })
	    })
	    .then(res => res.json())
	    .then(data => {

	      if(data){
	          Swal.fire({
	            title: "Thank you for your purchase!",
	            icon: "success",
	            text: `Order ID: ${data._id}`
	          }).then(reload => window.location.reload())
	     
	      } else {
	          Swal.fire({
	            title: "Please check order details",
	            icon: "info",
	            text: `Product ID: ${productId}	Name: ${name}`
	          })
	      }
	    })
	    }

  useEffect(() => {
      if(newQuantity !== "" && newQuantity !== '0'){
        setIsActive(true)
      }else {
        setIsActive(false)
      }
    }, [newQuantity])
  
	useEffect(() => {
		getProduct()
	}, [productId])

	return (

		<Container>
			<Row>
			  <Col xs={4} md={4}>
			    <Card className=" my-3">
			        <Card.Body>
			          <Card.Title>{name}</Card.Title>
		              <Card.Subtitle>Description:</Card.Subtitle>
		              <Card.Text>{description}</Card.Text>
		              <Card.Subtitle>Price:</Card.Subtitle>
		              <Card.Text>{price}</Card.Text>
		            <Card.Subtitle>Quantity:</Card.Subtitle>
		               <Form.Group className="mb-3" controlId="fullName">
				            <Form.Label>Name</Form.Label>
				            <Form.Control 
				              type="text"
				              value={newQuantity}
				              onChange={(e) => {setNewQuantity(e.target.value)}}
				              placeholder={quantity}
				              required
				              />
				          </Form.Group>
			          <div className="d-grip gap-2">
			          {
			          		(isActive) ?
			          			<>
			          			<Button onClick={() => addOrder()}>Buy</Button>
			          			<Button className="bg-primary ms-2" as={Link} to="/products">Back</Button>
			          			</>
			          			:
			          			<>
			          			<Button onClick={() => addOrder()} disabled>Buy</Button> 
			          			<Button className="bg-primary ms-2" as={Link} to="/products">Back</Button>
			          		</>
			          }
			          </div>
			          </Card.Body>
			      </Card>
			  </Col>
			</Row>
		</Container>

	)

}