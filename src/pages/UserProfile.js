import { useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function UserProfile() {

	const { user } = useContext(UserContext)

	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")

	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(password1 !== "" && password2 !== "" && (password1 === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [password1, password2])

	console.log(user.email)

	return (
		<Row>
			<Col className="p-5">
				<h1>{user.fullname}</h1>
				<h2>{user.email}</h2>

			      <Form.Group className="mt-5" controlId="password1">
			        <Form.Label>New Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Enter New Password"/>
			      </Form.Group>

			   
			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify New Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Verify New Password"/>
			      </Form.Group>

			      { isActive ?
			      			<Button variant="primary" onClick={() => window.location.reload()}>
			      		 	 Change Password
			      			</Button>
			      			:
			      			<Button variant="danger" onClick={() => window.location.reload()} disabled>
			      			  Change Password
			      			</Button>
			      }

			</Col>
		</Row>


		)
}