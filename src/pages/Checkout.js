import { useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Card, Container, Modal, Form } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function CheckoutProduct({getId, getName, getPrice, setModal}){
  console.log(getName)
	const {user} = useContext(UserContext)

  const [isActive, setIsActive] = useState(false)

  const [show, setShow] = useState(true)
  

	const [quantity, setQuantity] = useState("")

  const navigate = useNavigate()

  function addOrder() {
    fetch(`${process.env.REACT_APP_API_URL}/products/addOrders`, {
      method: "POST",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        buyer: [{
          fullname: user.fullname,
          userId: user.id,
          userEmail: user.email
        }],
        product: [{
          name: getName,
          productId: getId,
          quantity: quantity,
          price: getPrice          
        }]
      })
    })
    .then(res => res.json())
    .then(data => {

        if(data){
          Swal.fire({
            title: "Product Added!",
            icon: "success",
            text: "Product successfully added!"
          })
        navigate('/productMaintenance')
      } else {
          Swal.fire({
            title: "Product NOT Added",
            icon: "info",
            text: "Please check product information"
          })
      }
    })
    }

  useEffect(() => {
      if(quantity !==""){
        setIsActive(true)
      }else {
        setIsActive(false)
      }
    }, [quantity])


	return (
    <>
	<Container class="col-md-12 text-center">
    <Modal size="lg" show={() => setModal(true)} onHide={() => setModal(false)} aria-labelledby="example-modal-sizes-title-sm">
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-sm">CheckOut</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form.Group className="mb-3" controlId="fullName">
            <Form.Label>Name</Form.Label>
            <Form.Control 
              type="text"
              value={quantity}
              onChange={(e) => {setQuantity(e.target.value)}}
              placeholder="Enter quantity"
              required
              />
          </Form.Group>
      </Modal.Body>
   
    <Modal.Footer>
        { (isActive) ?
         <Button variant="primary" onClick={(e) => {addOrder(e); setModal(false);}}>
         Checkout
        </Button>
        :
          <Button variant="primary" onClick={(e) => {addOrder(e); setModal(false);}} disabled>
            Checkout
          </Button>
        }
        <Button onClick={() => setModal(false)}>Close</Button>
      </Modal.Footer>
    </Modal>
  </Container>
  </>
		)
}